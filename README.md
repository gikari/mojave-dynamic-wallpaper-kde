# Mojave Dynamic Wallpaper for KDE Plasma

This is default wallpaper from macOS Mojave adapted to [KDE Plasma dynamic wallpaper plugin](https://github.com/zzag/dynamic-wallpaper).

![Preview](preview.jpeg)

## Installation 

Drop metadata.json and images directory into ~/.local/share/dynamicwallpapers/Mojave, or use this quick installation script:

```
git clone https://gitlab.com/gikari/mojave-dynamic-wallpaper-kde.git Mojave
mkdir -p ~/.local/share/dynamicwallpapers
cp -pr Mojave ~/.local/share/dynamicwallpapers/
```

After that there will be an option in the plugin's configuration window.

## Acknowledgments

- 16 different wallpapers grabbed from [here](https://www.reddit.com/r/MacOS/comments/8oz5h5/all_16_full_resolution_macos_mojave_dynamic/). 
- Azimuth and elevation values grabbed from [here](https://itnext.io/macos-mojave-dynamic-wallpaper-fd26b0698223).